<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @since      2018.1.0
 *
 * @package    Acf_To_Php_Plugin
 * @subpackage Acf_To_Php_Plugin/Includes
 */

namespace Acf_To_Php_Plugin\Includes;
/**
 * The core plugin class.
 *
 * This is used to define admin-specific hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      2018.1.0
 * @package    Acf_To_Php_Plugin
 * @subpackage Acf_To_Php_Plugin/Includes
 * @author     Richard Korthuis - Acato <richardkorthuis@acato.nl>
 */
class Plugin {

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    2018.1.0
	 * @access   protected
	 * @var      string $plugin_name The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    2018.1.0
	 * @access   protected
	 * @var      string $version The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies and set the hooks for the admin area.
	 *
	 * @since    2018.1.0
	 */
	public function __construct() {
		$this->version     = '2021.1.0';
		$this->plugin_name = 'acf-to-php';

		$this->define_admin_hooks();
		$this->set_locale();
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    2018.1.0
	 * @access   private
	 */
	private function define_admin_hooks() {
		$admin = new \Acf_To_Php_Plugin\Admin\Admin( $this->get_plugin_name(), $this->get_version() );

		add_action( 'acf/update_field_group', array( $admin, 'update_field_group' ), 10, 1 );
		add_action( 'acf/duplicate_field_group', array( $admin, 'update_field_group' ), 10, 1 );
		add_action( 'acf/untrash_field_group', array( $admin, 'update_field_group' ), 10, 1 );
		add_action( 'acf/trash_field_group', array( $admin, 'delete_field_group' ), 10, 1 );
		add_action( 'acf/delete_field_group', array( $admin, 'delete_field_group' ), 10, 1 );
		add_action( 'init', array( $admin, 'include_php_folders' ), 10, 0 );
		add_action( 'admin_init', array( $admin, 'auto_sync' ), 999 );
		add_action( 'admin_notices', array( $admin, 'auto_sync_warning' ) );

		// For the options page
		add_action( 'admin_menu', array( $admin, 'add_admin_menu' ), 50 );
		add_action( 'admin_init', array( $admin, 'register_plugin_settings' ), 10 );
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     2018.1.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     2018.1.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Acf_To_Php_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    2018.1.2
	 * @access   private
	 */
	private function set_locale() {
		$plugin_i18n = new I18n();
		add_action( 'plugins_loaded', array( $plugin_i18n, 'load_plugin_textdomain' ) );
	}
}
