<?php
/**
 * Class Inference - for inferring stuff in case not provided.
 *
 * @since      version next
 * @package    acf-to-php
 * @subpackage includes
 */

namespace Acf_To_Php_Plugin\Includes;

class Inference {
	/**
	 * Regex to match gettext functions.
	 */
	const GETTEXT_REGEX = '(__|_x|esc_html_e|esc_html__)';

	/**
	 * Const to use as a token to split the line into parts. Quite randomly chosen so it should not appear in the code.
	 * Except it appears in this file, so we use it prefixed elsewhere.
	 */
	private const GETTEXT_TOKEN = '2389047238947021374923785423784623497235467234';

	/**
	 * Infer the text domain from the files.
	 *
	 * @param array $files The files to infer the text domain from.
	 *
	 * @return string|false The text domain or false if not found.
	 */
	public static function infer_text_domain_from_files( $files ) {
		// get code, find lines with `__(`, `_x(`, `esc_html_e`, `esc_html__`, get the text domain from them
		// if there are multiple text domains, return the most common one
		// if there are no text domains, return false
		$lines = [];
		foreach ( $files as $file ) {
			$lines = array_merge( $lines, array_filter( file( $file ), [ self::class, 'filter_only_lines_with_gettext_functions' ] ) );
		}

		$text_domains = [];
		foreach ( $lines as $line ) {
			$text_domain = self::get_text_domain_from_line( $line );
			if ( $text_domain ) {
				$text_domains[] = $text_domain;
			}
		}

		if ( empty( $text_domains ) ) {
			return false;
		}
		$counts = array_count_values( $text_domains );
		arsort( $counts );
		$most_common_text_domain = key( $counts );
		if ( 1 < count( $counts ) ) {
			error_log( 'Multiple text domains found: ' . implode( ', ', array_keys( $counts ) ) );
		}

		return $most_common_text_domain;
	}

	/**
	 * Filter only lines with gettext functions.
	 *
	 * @param string $line The line of code.
	 *
	 * @return bool
	 */
	private static function filter_only_lines_with_gettext_functions( $line ) {
		return preg_match( '/' . self::GETTEXT_REGEX . '\s*\(/', $line );
	}

	/**
	 * Get the text domain from a line of code.
	 *
	 * @param string $line The line of code.
	 *
	 * @return string|false The text domain or false if not found.
	 */
	private static function get_text_domain_from_line( $line ) {
		// Rules:
		// a textdomain cannot contain a quote, comma or space so we have some leeway.
		// a textdomain is always the last argument of the function.
		// we don't care about any other argument.
		// we could be dealing with a split line, but we only get one line, so we can't know.

		// Convert quotes to single quotes, only if not escaped
		$line = preg_replace( '/(?<!\\\\)"/', "'", $line );
		// remove spacing between parameters
		$line = preg_replace( '/\s*,\s*/', ',', $line );
		// split the line into parts; in case of a bad developer, we must assume the function call might not be alone on the line;
		$line = preg_replace( '/' . self::GETTEXT_REGEX . '\s*\(/', 'GETTEXTTOKEN' . self::GETTEXT_TOKEN, $line );
		$line = explode( 'GETTEXTTOKEN' . self::GETTEXT_TOKEN, $line );
		$line = $line[1] ?? '';
		if ( ! $line ) {
			return false;
		}
		// We now have a line like this; `'My string','My Context','My Text Domain');` or similar.
		// We need to extract the last argument.
		$line = explode( ',', $line );
		// Get the last argument, and only between the first and last quote.
		$section = array_pop( $line );
		while ( $line && ( ! $section || ! str_contains( $section, ")" ) ) ) {
			$section = array_pop( $line );
		}
		$line = explode( "'", $section );
		$line = $line[1] ?? '';
		if ( ! $line ) {
			return false;
		}

		return $line;
	}

	/**
	 * Find files in a directory, recursively, based on a pattern, with a max number of files.
	 * Only files eligible for scanning are returned. Third party code is excluded as much as possible.
	 */
	public static function find_files( $dir, $pattern = '*.php', $max_files = 25, $max_depth = 5, $current_depth = 0 ) {
		$files  = [];
		$ignore = [
			'vendor',
			'node_modules',
			'acf-php',
		];
		if ( $current_depth > $max_depth || $max_files <= 0 || in_array( basename( $dir ), $ignore, true ) ) {
			return $files;
		}
		$entries = glob( $dir . '/' . $pattern );
		// List files first.
		foreach ( $entries as $entry ) {
			if ( is_file( $entry ) ) {
				$files[] = $entry;
				if ( count( $files ) >= $max_files ) {
					break;
				}
			}
		}

		$entries = glob( $dir . '/' . '*', GLOB_ONLYDIR );
		// Then recurse into directories.
		foreach ( $entries as $entry ) {
			if ( is_dir( $entry ) ) {
				$files = array_merge( $files, self::find_files( rtrim( $entry, '/' ), $pattern, $max_files - count( $files ), $max_depth, $current_depth + 1 ) );
				if ( count( $files ) >= $max_files ) {
					break;
				}
			}
		}

		return $files;
	}
}
