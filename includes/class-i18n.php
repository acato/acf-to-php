<?php
/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link:
 * @since      2018.1.2
 *
 * @package    Acf_To_Php_Plugin
 * @subpackage Acf_To_Php_Plugin/Includes
 */

namespace Acf_To_Php_Plugin\Includes;
/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      2018.1.2
 * @package    Acf_To_Php_Plugin
 * @subpackage Acf_To_Php_Plugin/Includes
 * @author     Richard Korthuis - Acato <richardkorthuis@acato.nl>
 */
class I18n {

    /**
     * Load the plugin text domain for translation.
     *
     * @since    2018.1.2
     */
    public function load_plugin_textdomain() {
        load_plugin_textdomain(
            'acf-to-php',
            false,
            dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
        );

    }
}
