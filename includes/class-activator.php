<?php
/**
 * Fired during plugin activation
 *
 * @link:
 * @since      2018.1.2
 *
 * @package    Acf_To_Php_Plugin
 * @subpackage Acf_To_Php_Plugin/Includes
 */

namespace Acf_To_Php_Plugin\Includes;
/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      2018.1.2
 * @package    Acf_To_Php_Plugin
 * @subpackage Acf_To_Php_Plugin/Includes
 * @author     Richard Korthuis - Acato <richardkorthuis@acato.nl>
 */
class Activator {

    /**
     * Create the default "acf-php" directory in the active theme root
     * if the directory does not exist.
     *
     * @since     2018.1.1
     * @return    bool        If the directory exixts or is succesfully created
     */
    public static function activate() {
        $path = self::get_path();
        if ( ! is_dir( $path ) ) {
            return mkdir( $path, 0755 );
        }

        return true;
    }

    /**
     * Retrieve the default directory name for saving php files.
     *
     * @since     2018.1.1
     * @return    string    The default directory name for saving php files.
     */
    public static function get_path() {
        $path = apply_filters( 'acf_to_php/save_php', get_template_directory() . DIRECTORY_SEPARATOR . ACF_TO_PHP_DIRECTORY );

        return $path;
    }
}
