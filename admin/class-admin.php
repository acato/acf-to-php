<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @since      2018.1.0
 *
 * @package    Acf_To_Php_Plugin
 * @subpackage Acf_To_Php_Plugin/Admin
 */

namespace Acf_To_Php_Plugin\Admin;

use Acf_To_Php_Plugin\Includes\Inference;
use Text_Diff;
use Text_Diff_Op_change;
use Text_Diff_Op_copy;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version and functions for the admin-specific functionality.
 *
 * @package    Acf_To_Php_Plugin
 * @subpackage Acf_To_Php_Plugin/Admin
 * @author     Richard Korthuis - Acato <richardkorthuis@acato.nl>
 */
class Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    2018.1.0
	 * @access   private
	 * @var      string $plugin_name The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    2018.1.0
	 * @access   private
	 * @var      string $version The current version of this plugin.
	 */
	private $version;

	/**
	 * The origin for php based field groups.
	 *
	 * @since    2018.1.2
	 * @access   public
	 * @var      array $origins An array containing field group as key and path as value.
	 */
	public static $origins = array();

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    2018.1.0
	 *
	 * @param string $plugin_name The name of this plugin.
	 * @param string $version     The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version     = $version;

		self::$origins = self::glob_for_acf_php_files();

		add_action( 'admin_footer', [ $this, 'add_origin_to_table_script' ] );
	}

	/**
	 * Preload the origins with actual file data.
	 * The filter will still be able to overrule.
	 *
	 * @return array
	 */
	private static function glob_for_acf_php_files() {
		$starting_dir = defined( 'WP_CONTENT_DIR' ) ? WP_CONTENT_DIR : dirname( __FILE__, 4 );
		$paths        = glob( $starting_dir . '/*/*/acf-php/*.php' );
		$origins      = array();
		foreach ( $paths as $path ) {
			$group_name             = basename( $path, '.php' );
			$origins[ $group_name ] = dirname( $path );
		}

		return $origins;
	}

	/**
	 * Get a list of differences between two strings, used to determine if a field group has changed, ignoring the 'modified' field.
	 *
	 * @param string $left_string  The left string to compare.
	 * @param string $right_string The right string to compare.
	 *
	 * @return array An array of differences.
	 */
	private static function get_differences( $left_string, $right_string ) {
		if ( ! class_exists( 'WP_Text_Diff_Renderer_Table', false ) ) {
			require ABSPATH . WPINC . '/wp-diff.php';
		}

		$left_string  = normalize_whitespace( $left_string );
		$right_string = normalize_whitespace( $right_string );

		$left_lines  = explode( "\n", $left_string );
		$right_lines = explode( "\n", $right_string );
		$text_diff   = new Text_Diff( 'auto', [ $left_lines, $right_lines ] );
		if ( empty( $text_diff->_edits ) ) {
			return [];
		}
		$differences = [];
		foreach ( $text_diff->_edits as $edit ) {
			if ( $edit instanceof Text_Diff_Op_change ) {
				// This is an actual change, but, it could be just the 'modified' field, so we need to check the actual content.
				// If the change is really just the modified field, we can ignore it, as it's not a real change.
				// The list of conditions is long, so; change needs to be present, exactly one line, and the change needs to be the 'modified' field.
				if (
					! empty( $edit->orig ) && ! empty( $edit->final ) && // We have both original and final content.
					is_array( $edit->orig ) && is_array( $edit->final ) && // Both are arrays.
					count( $edit->orig ) === count( $edit->final ) && // Both have the same number of lines.
					count( $edit->orig ) === 1 && // There is exactly one line changed.
					// And that exact one change is the 'modified' field.
					preg_match( "/^'modified' => [0-9]+,$/", trim( $edit->orig[0] ) ) && preg_match( "/^'modified' => [0-9]+,$/", trim( $edit->final[0] ) )
				) {
					// We ignore this change so we can say "If there are no changes" later.
					continue;
				}
			}
			// Skip "copy" as it's not a change.
			if ( $edit instanceof Text_Diff_Op_copy ) {
				continue;
			}
			// Everything else is a change. This includes Text_Diff_Op_add and Text_Diff_Op_delete.
			$differences[] = $edit;
		}

		return $differences;
	}

	/**
	 * Automatically update field groups in database if acf-php files have changed.
	 *
	 * @since   2021.1.0
	 */
	public function auto_sync() {
		if ( ! get_option( 'acf_to_php_enable_sync' ) || ! function_exists( 'acf_get_field_groups' ) ) {
			return;
		}
		$field_groups = acf_get_field_groups();
		foreach ( $field_groups as $field_group ) {
			if ( 'php' !== acf_maybe_get( $field_group, 'local' ) ) {
				continue;
			}

			if ( ! $field_group['ID'] ) {
				$posts = get_posts(
					[
						'name'        => $field_group['key'],
						'post_type'   => 'acf-field-group',
						'post_status' => 'publish',
					]
				);
				if ( count( $posts ) ) {
					$field_group['ID'] = $posts[0]->ID;
				}
			}

			if ( $field_group['ID'] ) {
				$modified = acf_maybe_get( $field_group, 'modified' );
				if ( ! $modified || $modified <= get_post_modified_time( 'U', true, $field_group['ID'] ) ) {
					continue;
				}
			}

			// load fields
			$field_group['fields'] = acf_get_fields( $field_group );

			acf_import_field_group( $field_group );
		}
	}

	public function auto_sync_warning() {
		$screen = get_current_screen();

		if ( ! in_array( $screen->id, [ 'edit-acf-field-group', 'acf-field-group' ] ) ) {
			return;
		}

		if ( ! get_option( 'acf_to_php_enable_sync' ) ) {
			?>
			<div class="notice notice-warning">
				<p>
					<strong>ACF to PHP:</strong>
					<?php
					// translators: %s is a link to the settings page.
					printf( __( 'WARNING! You are editing ACF groups without ACF to PHP sync enabled. Please activate <a href="%s">Settings > ACF to PHP > "Enable sync"</a> to make sure you are working with the latest version of the ACF field groups.', 'acf-to-php' ), admin_url( 'options-general.php?page=acf-to-php-options' ) );
					?>
				</p>
			</div>
			<?php
			return;
		}

		$group  = get_post( $_GET['post'] )->post_name ?? '';
		$origin = self::$origins[ $group ] ?? '';
		if ( $origin ) {
			$origin = str_replace( trailingslashit( WP_CONTENT_DIR ), '', $origin ) . '/' . $group . '.php';
			?>
			<div class="notice notice-info"><p>
					<?php
					// translators: %s is the (shortened) path to the file.
					printf( __( 'This data is stored in <code>%s</code>', 'acf-to-php' ), $origin );
					?>
				</p></div>
			<?php
		}
	}

	/**
	 * This function is used to create the settings page for Acf_To_Php
	 *
	 * @since   2018.1.1
	 * @return  void
	 */
	public function add_admin_menu() {
		add_options_page(
			__( 'ACF to PHP Options', 'acf-to-php' ),
			__( 'ACF to PHP', 'acf-to-php' ),
			'manage_options',
			'acf-to-php-options',
			array( $this, 'settings_page' )
		);

		add_submenu_page(
			'edit.php?post_type=acf-field-group',
			__( 'ACF to PHP Options', 'acf-to-php' ),
			__( 'ACF to PHP Settings', 'acf-to-php' ),
			'manage_options',
			'options-general.php?page=acf-to-php-options',
			array( $this, 'settings_page' )
		);
	}

	/**
	 * This function is used to create the settings group for Acf_To_Php
	 *
	 * @since   2018.1.1
	 * @return  void
	 */
	public function register_plugin_settings() {
		register_setting( 'acf-to-php-settings-group', 'acf_to_php_enable_sync' );
		register_setting( 'acf-to-php-settings-group', 'acf_to_php_enable_translatable_strings' );
	}

	/**
	 * This function add the html for the ACF to PHP options page
	 *
	 * @since   2018.1.1
	 * @return  void
	 */
	public function settings_page() {
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'ACF to PHP Options', 'acf-to-php' ); ?></h1>
			<form method="post" action="options.php">
				<?php settings_fields( 'acf-to-php-settings-group' ); ?>
				<?php do_settings_sections( 'acf-to-php-settings-group' ); ?>
				<table class="form-table">
					<tr valign="top">
						<th scope="row"><?php esc_html_e( 'Enable sync?', 'acf-to-php' ); ?></th>
						<td><input
								type="checkbox" name="acf_to_php_enable_sync"
								value="1"<?php echo (int) get_option( 'acf_to_php_enable_sync' ) === 1 ? ' checked' : ''; ?>/>
						</td>
					</tr>
					<tr valign="top">
						<th scope="row"><?php esc_html_e( 'Enable translatable strings?', 'acf-to-php' ); ?></th>
						<td><input
								type="checkbox" name="acf_to_php_enable_translatable_strings"
								value="1"<?php echo (int) get_option( 'acf_to_php_enable_translatable_strings' ) === 1 ? ' checked' : ''; ?>/>
						</td>
					</tr>
				</table>
				<?php submit_button(); ?>
			</form>
		</div>
		<?php
	}

	/**
	 * This function is hooked into the acf/update_field_group action and will save all field group data to a .php file
	 *
	 * @since   2018.1.0
	 *
	 * @param array $field_group
	 */
	public function update_field_group( $field_group ) {
		// validate
		if ( ! acf_get_setting( 'json' ) ) {
			return;
		}

		// get fields
		$field_group['fields'] = acf_get_fields( $field_group );

		// save file
		$this->write_php_field_group( $field_group );
	}

	/**
	 * This function will remove the field group .php file
	 *
	 * @since   2018.1.0
	 *
	 * @param array $field_group
	 */
	public function delete_field_group( $field_group ) {
		// WP appends '__trashed' to end of 'key' (post_name)
		$field_group['key'] = str_replace( '__trashed', '', $field_group['key'] );

		// delete
		$this->delete_php_field_group( $field_group['key'] );
	}

	/**
	 * This function will include all registered .php files
	 *
	 * @since   2018.1.0
	 */
	public function include_php_folders() {
		// vars
		$paths = apply_filters( 'acf_to_php/load_php', [ apply_filters( 'acf_to_php/save_php', $this->get_default_path() ) ] );

		// add custom origins with a filter
		self::$origins = apply_filters( 'acf_to_php/add_origin', self::$origins );

		// loop through and add to cache
		foreach ( $paths as $path ) {
			$this->include_php_folder( $path );
		}
	}

	/**
	 * This function will include all .php files within a folder
	 *
	 * @since   2018.1.0
	 *
	 * @param string $path
	 *
	 * @return  boolean
	 */
	private function include_php_folder( $path = '' ) {
		// Remove trailing slash.
		$path = untrailingslashit( $path );

		// Bail early if path does not exist.
		if ( ! is_dir( $path ) || ! is_writeable( $path ) ) {
			return false;
		}

		// Loop directory.
		foreach ( glob( $path . DIRECTORY_SEPARATOR . 'group_*.php', GLOB_NOSORT ) as $php_file ) {

			// Add the origin.
			$group_name                   = str_replace( '.php', '', basename( $php_file ) );
			self::$origins[ $group_name ] = $path;

			// Include the php file.
			include_once $php_file;
		}

		// Add custom origins with a filter. All files should already be here, if they are in the proper structure. This filter allows for non-standard locations.
		self::$origins = apply_filters( 'acf_to_php/add_origin', self::$origins );

		// return
		return true;
	}

	/**
	 * This function will save a field group to a php file within the current theme
	 *
	 * @since   2018.1.0
	 *
	 * @param array $field_group The field group to save.
	 *
	 * @return  boolean
	 */
	private function write_php_field_group( $field_group ) {
		$file         = $field_group['key'] . '.php';
		$str_replace  = array(
			'  '                => '    ',
			"'!!__(!!\'"        => "__('",
			"'!!_x(!!\'"        => "_x('",
			"!!\', !!\'"        => "', '",
			"!!\')!!'"          => "')",
			'array ('           => 'array(',
			"'local' => 'php'," => "'local' => apply_filters('acf_to_php/local', 'php'),",
		);
		$preg_replace = array(
			'/([\t\r\n]+?)array/'               => 'array',
			'/[0-9]+ => ([\r\n]+[\s]+?)?array/' => 'array',
		);

		// set the path
		self::$origins      = apply_filters( 'acf_to_php/add_origin', self::$origins );
		$field_group_origin = self::$origins[ $field_group['key'] ];
		if ( $field_group_origin ) {
			$path = $field_group_origin;
		} else {
			$path = $this->get_php_field_group_path( $field_group['key'] );
		}

		// bail if dir does not exist
		if ( ! is_writable( $path ) ) {
			return false;
		}

		// prepare for export
		$id          = acf_extract_var( $field_group, 'ID' );
		$field_group = acf_prepare_field_group_for_export( $field_group );

		// add modified time
		$field_group['modified'] = get_post_modified_time( 'U', true, $id, true );

		// mark it as php (will be replaced with filter option)
		$field_group['local'] = 'php';

		// add translatable strings with theme text domain from theme's style.css.
		if ( $field_group_origin && get_option( 'acf_to_php_enable_translatable_strings' ) ) {
			// Check if file is in child theme.
			if ( str_contains( $field_group_origin, '/themes/' . get_stylesheet() . '/' ) ) {
				$locale_textdomain = wp_get_theme()->get( 'TextDomain' );
				// Check if file is in parent theme.
			} elseif ( wp_get_theme()->parent() && str_contains( $field_group_origin, '/themes/' . wp_get_theme()->parent()->get_stylesheet() . '/' ) ) {
				$locale_textdomain = wp_get_theme()->parent()->get( 'TextDomain' );
				// Check if file is in a theme.
			} elseif ( str_contains( $field_group_origin, '/themes/' ) ) {
				// get the text domain from the theme's style.css.
				list( $themes_parent, $theme_folder ) = explode( '/themes/', $field_group_origin );
				$theme_name        = explode( '/', trim( $theme_folder, '/' ) )[0];
				$theme_data        = wp_get_theme( $theme_name );
				$locale_textdomain = $theme_data->get( 'TextDomain' );
				// Check if file is in a plugin.
			} else {
				$plugin_dir  = dirname( str_replace( realpath( WP_CONTENT_DIR . '/plugins' ) . '/', '', realpath( $field_group_origin ) ) );
				$plugin_data = [];
				foreach ( glob( WP_CONTENT_DIR . '/plugins/' . $plugin_dir . '/*.php' ) as $plugin_file ) {
					$plugin_data = get_plugin_data( $plugin_file );
					if ( $plugin_data && $plugin_data['TextDomain'] ) {
						break;
					}
				}
				$locale_textdomain = $plugin_data['TextDomain'] ?? false;
			}

			$notice = '';

			// Last chance;
			if ( ! $locale_textdomain ) {
				$files             = Inference::find_files( dirname( $field_group_origin ) ); // Indeed, this depends on acf-php folder being at level 0 of the plugin/theme.
				$locale_textdomain = Inference::infer_text_domain_from_files( $files );
				$notice            = '// Warning: We had to infer the text domain from the files. Please check if this is correct and set the text domain in the theme/plugin header.' . "\r\n";
			}

			// Fallback to 'acf-to-php' if no text domain is found.
			if ( ! $locale_textdomain ) {
				$locale_textdomain = 'acf-to-php';
			}

			$field_group['locale_textdomain'] = $locale_textdomain;

			$field_group = $this->get_translatable_strings( $field_group, $locale_textdomain );
		}

		// Prevent default translation and fake _x() within string.
		acf_update_setting( 'l10n_var_export', true );

		$php = "<?php // phpcs:disable" . "\r\n" . $notice;

		$php .= "if( function_exists('acf_add_local_field_group') ):" . "\r\n" . "\r\n";

		// code
		$code = var_export( apply_filters( 'acf_to_php/before_export', $field_group ), true );

		// correctly formats "=> array("
		$code = preg_replace( array_keys( $preg_replace ), array_values( $preg_replace ), $code );

		// change double spaces to tabs
		$code = str_replace( array_keys( $str_replace ), array_values( $str_replace ), $code );


		$php .= "acf_add_local_field_group({$code});" . "\r\n" . "\r\n";

		$php .= '// Automatically set the correct origin for this file.' . "\r\n";
		$php .= "add_filter( 'acf_to_php/add_origin', function( \$origins ) {" . "\r\n";
		$php .= "    \$origins[ basename( __FILE__, '.php' ) ] = __DIR__;" . "\r\n";
		$php .= "    return \$origins;" . "\r\n";
		$php .= "});" . "\r\n" . "\r\n";

		$php .= 'endif;';

		// create translatable strings in generated php file
		if ( get_option( 'acf_to_php_enable_translatable_strings' ) ) {
			preg_match_all( '/\'(_x\(.+\))\',/', $php, $matches );
			$matches[1] = array_map( function ( $match ) {
				return stripslashes( $match ) . ',';
			}, $matches[1] );

			$php = str_replace( $matches[0], $matches[1], $php );
		}

		// Check if the file exists and if it has changed, only allowed change is 1x 'modified', use wp_text_diff to determine if the file has changed.
		$old_php = file_exists( "{$path}/{$file}" ) ? file_get_contents( "{$path}/{$file}" ) : '';
		if ( ! self::get_differences( $old_php ?: '', $php ) ) {
			return false;
		}

		// write file
		$f = fopen( "{$path}/{$file}", 'w' );
		fwrite( $f, apply_filters( 'acf_to_php/before_save', $php ) );
		fclose( $f );

		// delete auto-generated json file, we don't need it
		acf_delete_json_field_group( $field_group['key'] );

		// return
		return true;
	}

	/**
	 * This function will create translatable strings for every field group, field and sub field.
	 *
	 * @param array  $field_group
	 * @param string $text_domain
	 *
	 * @return  array $field_group
	 */
	private function get_translatable_strings( $field_group, $text_domain ) {

		// Create translatable strings for title. The ACF context is used to prevent conflicts with other plugins.
		$field_group['title'] = '_x(\'' . $field_group['title'] . '\', \'ACF\', \'' . $text_domain . '\')';

		if ( $field_group['fields'] ) {
			foreach ( $field_group['fields'] as $key => $field ) {
				// create translatable strings for every field choice label.
				if ( isset( $field_group['fields'][ $key ]['choices'] ) ) {
					foreach ( $field_group['fields'][ $key ]['choices'] as $choice_key => $choice ) {
						$field_group['fields'][ $key ]['choices'][ $choice_key ] = $this->create_translatable_field( $choice, $text_domain );
					}
				}

				$fields_to_make_translatable = [
					'label',
					'instructions',
					'placeholder',
					'prepend',
					'append',
					'button_label',
					'message',
					'custom_choice_button_text',
				];

				foreach ( $fields_to_make_translatable as $field_to_make_translatable ) {
					if ( isset( $field_group['fields'][ $key ][ $field_to_make_translatable ] ) ) {
						$field_group['fields'][ $key ][ $field_to_make_translatable ] = $this->create_translatable_field( $field_group['fields'][ $key ][ $field_to_make_translatable ], $text_domain );
					}
				}

				// create translatable strings for every field which has sub fields.
				if ( isset( $field_group['fields'][ $key ]['sub_fields'] ) ) {
					foreach ( $field_group['fields'][ $key ]['sub_fields'] as $sub_field_key => $sub_field ) {
						// create translatable strings for every field sub field choice label.
						if ( $sub_field['choices'] ) {
							foreach ( $sub_field['choices'] as $sub_field_choice_key => $sub_field_choice ) {
								$field_group['fields'][ $key ]['sub_fields'][ $sub_field_key ]['choices'][ $sub_field_choice_key ] = $this->create_translatable_field( $sub_field_choice, $text_domain );
							}
						}

						foreach ( $fields_to_make_translatable as $field_to_make_translatable ) {
							if ( isset( $sub_field[ $field_to_make_translatable ] ) ) {
								$field_group['fields'][ $key ]['sub_fields'][ $sub_field_key ][ $field_to_make_translatable ] = $this->create_translatable_field( $sub_field[ $field_to_make_translatable ], $text_domain );
							}
						}
					}
				}

				// create translatable strings for every layout field which has sub fields.
				if ( isset( $field_group['fields'][ $key ]['layouts'] ) ) {
					foreach ( $field_group['fields'][ $key ]['layouts'] as $layout_key => $layout ) {
						if ( isset( $layout['sub_fields'] ) ) {
							foreach ( $layout['sub_fields'] as $sub_field_key => $sub_field ) {
								// create translatable strings for every field sub field choice label.
								if ( $sub_field['choices'] ) {
									foreach ( $sub_field['choices'] as $sub_field_choice_key => $sub_field_choice ) {
										$field_group['fields'][ $key ]['layouts'][ $layout_key ]['sub_fields'][ $sub_field_key ]['choices'][ $sub_field_choice_key ] = $this->create_translatable_field( $sub_field_choice, $text_domain );
									}
								}

								foreach ( $fields_to_make_translatable as $field_to_make_translatable ) {
									if ( isset( $sub_field[ $field_to_make_translatable ] ) ) {
										$field_group['fields'][ $key ]['layouts'][ $layout_key ]['sub_fields'][ $sub_field_key ][ $field_to_make_translatable ] = $this->create_translatable_field( $sub_field[ $field_to_make_translatable ], $text_domain );
									}
								}
							}
						}
					}
				}
			}
		}

		return $field_group;
	}

	/**
	 * This function will create a translatable field.
	 *
	 * @param string $label       The label to translate.
	 * @param string $text_domain The text domain to use for the translation.
	 *
	 * @return string|null
	 */
	private function create_translatable_field( $label, $text_domain ) {
		if ( empty( $label ) ) {
			return null;
		}

		$label = str_replace( "'", "\'", $label );

		return '_x(\'' . $label . '\', \'ACF\', \'' . $text_domain . '\')';
	}

	/**
	 * This function will delete a php field group file
	 *
	 * @since   2018.1.0
	 *
	 * @param string $key
	 *
	 * @return  boolean
	 */
	private function delete_php_field_group( $key ) {
		$file = $key . '.php';
		$path = $this->get_php_field_group_path( $key );

		// Bail early if file does not exist.
		if ( ! is_readable( "{$path}/{$file}" ) ) {
			return false;
		}

		// Remove file.
		unlink( "{$path}/{$file}" );

		return true;
	}

	/**
	 * This function will get the file path for a php field group file
	 *
	 * @since   2018.1.1
	 *
	 * @param string $key
	 *
	 * @return  string      The path to the file
	 */
	private function get_php_field_group_path( $key ) {
		// default path
		$path = apply_filters( 'acf_to_php/save_php', $this->get_default_path() );

		// if we have an original and existing origin, we overwrite!
		if ( ! empty( self::$origins[ $key ] ) && is_dir( self::$origins[ $key ] ) && is_writable( self::$origins[ $key ] ) ) {
			$path = self::$origins[ $key ];
		}

		// remove trailing slash
		$path = untrailingslashit( $path );

		return $path;
	}

	/**
	 * This function will return the default absolute path for php files
	 *
	 * @since   2018.1.2
	 * @return  string      The absolute path to the php file directory
	 */
	private function get_default_path() {
		return get_stylesheet_directory() . DIRECTORY_SEPARATOR . ACF_TO_PHP_DIRECTORY;
	}

	/**
	 * Add origin information to the field groups panel
	 *
	 * @return void
	 */
	public function add_origin_to_table_script() {
		global $pagenow;
		if ( 'edit.php' === $pagenow && isset( $_GET['post_type'] ) && 'acf-field-group' === $_GET['post_type'] ) {
			$acfphp = [ 'origins' => self::$origins ];
			?>
			<script id="acf-php-extra-data">
				var acfphp = <?php print wp_json_encode( $acfphp ); ?>;
				// for every cell with class .column-acf-key get the text value (group_id) and add as text the path to the php file.
				document.querySelectorAll('td.column-acf-key').forEach(function (cell) {
					var group_id = cell.innerText;
					if (acfphp.origins[group_id]) {
						var origin = acfphp.origins[group_id];
						// change the origin; if it contains 'themes/' we want to show 'Theme: ' + the theme name, otherwise 'Plugin: ' + the plugin name.
						if (origin.includes('themes/')) {
							origin = 'Theme: ' + origin.split('themes/')[1].split('/')[0];
						} else {
							origin = 'Plugin: ' + origin.split('plugins/')[1].split('/')[0];
						}
						cell.innerHTML += '<br><small>' + origin + '</small>';
					} else {
						cell.innerHTML += '<br><small>Unknown origin</small>';
					}
				});
			</script>
			<?php
		}
	}
}
