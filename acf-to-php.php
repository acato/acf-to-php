<?php

/**
 * @since             2018.1.0
 * @package           Acf_To_Php_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       ACF to PHP
 * Description:       Save ACF to PHP files
 * Version:           2025.3.0
 * Author:            Richard Korthuis - Acato
 * Author URI:        https://www.acato.nl
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * Default directory name for saving .php files
 */
define( 'ACF_TO_PHP_DIRECTORY', 'acf-php' );

require_once plugin_dir_path( __FILE__ ) . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class-autoloader.php';
spl_autoload_register( [ '\Acf_To_Php_Plugin\Includes\Autoloader', 'autoload' ] );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-rest-cache-activator.php
 */
register_activation_hook( __FILE__, [ '\Acf_To_Php_Plugin\Includes\Activator', 'activate' ] );

/**
 * Begins execution of the plugin.
 */
new \Acf_To_Php_Plugin\Includes\Plugin();
