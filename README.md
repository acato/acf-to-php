# ACF to PHP #

Make Advanced Custom Fields not save its field groups to JSON files, but to PHP files to increase performance.

## Description ##

This plugin makes Advanced Custom Fields not save its field groups to JSON files but to PHP files. Loading the fields
through JSON is extremely bad for performance. Loading them through PHP increases performance, but this plugin still
allows you to sync the field groups back to the database for editing (when using the `acf_to_php/local`).

There are some hooks available to alter part of the functionality of this plugin (or how ACF treats the PHP):

**acf_to_php/save_php**

This hook can be used to tell the plugin where to save the PHP files. Default the files are saved to a directory
`/acf-php` inside the current theme (if it exists!). This hook can be used to change the default location, but keep in
mind only one location can be specified!

Usage:

    <?php
    function change_acf_php_location($location) {
        // change the directory to /my-own-acf-php inside the current theme
        return get_stylesheet_directory() . '/my-own-acf-php';
    }

    add_filter('acf_to_php/save_php', 'change_acf_php_location');
    ?>

**acf_to_php/load_php**

This hook can be used to load php from alternative locations (i.e. from a plugin directory). Off course since it is
loaded through php you can decide to skip this hook and load the php yourself.

Usage:

    <?php
    function add_acf_php_location($paths) {
        // add plugin acf php directory (assumes this function is within your plugin)
        $paths[] = plugin_dir_path( __FILE__ ) . 'acf-php';

        return $paths;
    }

    add_filter('acf_to_php/load_php', 'add_acf_php_location');
    ?>

**acf_to_php/local**

This hook can be used to make sure the ACF field groups can be synchronised to the database (default setting is that
they can not be synchronised, since you don't want any synchronisation on Accept or Live). With this hook you can change
the value of the `local` variable of an ACF group. Default this variable is set to `'php'`, which means ACF won't allow
you to sync the ACF group. Change it to `'json'` to enable synchronisation. It is probably best to only apply this hook
on a local/development environment.

Usage:

    <?php
    function make_acf_sync_possible($local) {
        return 'json';
    }

    add_filter('acf_to_php/local', 'make_acf_sync_possible');
    ?>

**acf_to_php/add_origin**

This hook can be used to add an origin to the ACF field group. This is useful when you want to know where the
field_group originates from, either a plugin or a theme. The origin is used to determine where changes to the
field_group should be saved. The origin is also used to determine which locale textdomain to use for the field_group.
This filter can be used either in a plugin and in a theme.

Please note that if you use the convention `acf-php` as storage for the files, the files are auto-detected and registered in the origins.
You can verify the origins in the ACF Field Groups interface. Use snippet below to correct if wrong.

Usage:

    <?php
    function acf_add_origin( $origins ) {
		foreach ( glob( __DIR__ . '/acf-php/*php' ) as $acf_file ) {
			$origins[ basename( $acf_file, '.php' ) ] = dirname( $acf_file );
		}
		return $origins;
	}

    add_filter('acf_to_php/add_origin', 'acf_add_origin' );
    ?>

## Installation ##

1. Upload to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Optionally apply some of the hooks

## Changelog ##

2025.3.0

* Released on 2025-03-04
* Added translation of labels and options in ACF files
* Translations will to the best of our ability use the correct text-domain, based on the location of the file
* Disabled PHPCS in acf php files so you don't have to exclude them in phpcs.xml
* Added Dutch translations. If you need a different language, please use the .pot file to create a translation and send it to us

2024.1.0

* unknown changes, changelog not maintained

2021.1.0

* unknown changes, changelog not maintained

2018.1.2

* unknown changes, changelog not maintained

2018.1.1

* unknown changes, changelog not maintained

2018.1.0

* Initial version.

## Developer notes; translations ##

Add new strings/update strings;

```bash
wp i18n make-pot --domain=acf-to-php --exclude=vendor,node_modules,languages . languages/acf-to-php.pot --skip-js; wp i18n update-po languages/*pot languages/*po
```

Process translations;

```bash
wp i18n make-mo languages/*.po languages/ ; wp i18n make-php languages/*.po languages/ ;
```
