��          t       �       �   
   �      �      �      �           )     B  &   X  �        Y  �  n               (     B      ^          �  2   �  �   �     �   ACF to PHP ACF to PHP Options ACF to PHP Settings Enable sync? Enable translatable strings? Richard Korthuis - Acato Save ACF to PHP files This data is stored in <code>%s</code> WARNING! You are editing ACF groups without ACF to PHP sync enabled. Please activate <a href="%s">Settings > ACF to PHP > "Enable sync"</a> to make sure you are working with the latest version of the ACF field groups. https://www.acato.nl Project-Id-Version: ACF to PHP 2024.1.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/acf-to-php
Last-Translator: Remon Pel <remon@acato.nl>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2025-02-21T07:40:15+00:00
PO-Revision-Date: 2025-02-21 08:41+0100
Language: nl_NL
X-Generator: Poedit 3.4.2
X-Domain: acf-to-php
 ACF naar PHP ACF naar PHP Opties ACF naar PHP Instellingen Synchronisatie inschakelen? Vertaalbare strings inschakelen? Richard Korthuis - Acato ACF naar PHP bestanden opslaan Deze gegevens worden opgeslagen in <code>%s</code> WAARSCHUWING! Je bewerkt ACF-groepen zonder ACF naar PHP-synchronisatie ingeschakeld. Activeer <a href="%s">Instellingen > ACF naar PHP > "Synchronisatie inschakelen"</a> om ervoor te zorgen dat u werkt met de nieuwste versie van de ACF-veldgroepen. https://www.acato.nl 